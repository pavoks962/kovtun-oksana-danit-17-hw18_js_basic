"use strict"

const client = {
    gender: 'female',
    age: 32,
    size: {
        weight: 54,
        height: 170,
    }
}

const newObj = {};

const clonClient = (obj) => {
    for (let key in obj) {
        if (obj[key] === null) {
            newObj[key] = null;
        } else 
        if (typeof obj[key] !== "object") {
            newObj[key] = obj[key];
        } else if (typeof obj[key] === "object") {
           newObj[key] = clonClient(obj[key]);
        } 
            } return newObj; }
          
            
clonClient(client)

console.log(newObj)

console.log(client)